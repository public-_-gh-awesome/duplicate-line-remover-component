from ghpythonlib.componentbase import executingcomponent as component
import Grasshopper, GhPython
import System
import Rhino
import rhinoscriptsyntax as rs

class MyComponent(component):
    
    def RunScript(self, lines):
        import rhinoscriptsyntax as rs
        def LineAttributes(listofLines):
            Line_Atributes={}
            i = 0
            for line in listofLines:
                line = rs.coercecurve(line)
                start =  rs.CurveStartPoint(line)
                end = rs.CurveEndPoint(line)
                Line_Atributes[str(i) ]=[start,end]
                i+=1
            return Line_Atributes
        attribureDictionary = LineAttributes(lines)
        def Analyser(Dict,list):
            recorder=[]
            for key in Dict:
                newDict =  Dict.copy()
                del newDict[key]
                for j in newDict:
                    if Dict[key] == newDict[j] or Dict[key]==newDict[j].reverse():
                        recorder.append(key)
            for item in recorder:
                del Dict[item]
            indexNumbers=[]
            for keyname in Dict:
                indexNumber= float(keyname)
                indexNumber= int(indexNumber)
                indexNumbers.append(indexNumber)
            return indexNumbers
        Duplicateindecies = Analyser(attribureDictionary,lines)
        pures=[]
        for i in Duplicateindecies:
            pures.append(lines[i])
        
        return (pures, Duplicateindecies)
